@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-3 p-5">
            {{-- <img src="/svg/freeCodeGramLogo.svg" class="rounded-circle"> --}}
            <img src="{{ $user->profile->profileImage() }}" class="rounded-circle w-100">
        </div>
        <div class="col-9 pt-5">
            <div>
                <div class="d-flex justify-content-between align-items-baseline">
                    <div class="d-flex align-items-center pb-4">
                        {{-- <h1>{{$user -> username}}</h1> --}}
                        <div class="h4">
                            {{$user -> username}}
                        </div>

                        <follow-button user-id="{{ $user->id }}" follows="{{ $follows }}"></follow-button>
                    </div>

                    @can('update', $user->profile)
                        <a href="/p/create">Add New Post</a>
                    @endcan
                </div>

                @can('update', $user->profile)
                    <a href="/profile/{{ $user->id }}/edit">Edit Profile</a>
                @endcan

            </div>
            <div class="d-flex">
                <div class="pr-5"><strong>{{ $postCount }}</strong> posts</div>
                <div class="pr-5"><strong>{{ $followersCount }}</strong> followers</div>
                <div class="pr-5"><strong>{{ $followingCount }}</strong> following</div>
            </div>
            <div class="pt-4 font-weight-bold">
                {{ $user->profile->title }}
            </div>
            <div>
                {{ $user->profile->description }}
            </div>
            <div>
                {{-- <a href="#" target="_blank">{{ $user->profile->url ?? "N/A" }}</a> --}}
                <a href="#" target="_blank">{{ $user->profile->url }}</a>
            </div>
        </div>
    </div>

    <div class="row pt-5">
        @foreach($user->posts as $post)
            <div class="col-4 pb-4">
                <a href="/p/{{ $post->id }}">
                    <img src="/storage/{{ $post->image }}" class="w-100"/>
                </a>
            </div>
        @endforeach

        {{-- <div class="col-4"> --}}
        {{--     <img src="https://scontent-sea1-1.cdninstagram.com/v/t51.2885-15/sh0.08/e35/p640x640/118605679_1451236268600542_2050874606066190756_n.jpg?_nc_ht=scontent-sea1-1.cdninstagram.com&_nc_cat=110&_nc_ohc=dVHJeoUv2BgAX_e96RE&oh=484232acc8d05d6033f36a7fca4e4bf0&oe=5F7F655E" class="w-100"/> --}}
        {{-- </div> --}}
        {{-- <div class="col-4"> --}}
        {{--     <img src="https://scontent-sea1-1.cdninstagram.com/v/t51.2885-15/sh0.08/e35/s640x640/118287795_1049613382163863_2826749017340034474_n.jpg?_nc_ht=scontent-sea1-1.cdninstagram.com&_nc_cat=107&_nc_ohc=bcbOvANNRUAAX9kJtKk&oh=d05e112c784ec42d922893c59bb4a532&oe=5F7FDDA4" class="w-100"/> --}}

        {{-- </div> --}}
        {{-- <div class="col-4"> --}}
        {{--     <img src="https://scontent-sea1-1.cdninstagram.com/v/t51.2885-15/sh0.08/e35/p640x640/97372557_2621920334794301_4298274250584486821_n.jpg?_nc_ht=scontent-sea1-1.cdninstagram.com&_nc_cat=105&_nc_ohc=ED76cN5UEVcAX_rVsli&oh=d80e57d5130872b9feb6623b4c664567&oe=5F7E6D22" class="w-100"/> --}}
        {{-- </div> --}}
    </div>
</div>
@endsection
