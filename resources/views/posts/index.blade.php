@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-3 offset-5"> 
            {{-- {{ dd($user_id) }} --}}
                <a href="/profile/{{ $user_id }}" style="color:white">
                    <button class="btn btn-success">Your Profile
                </a>
            </button>
        </div>
    </div>
    @foreach($posts as $post)
        <div class="row">
            <div class="col-6 offset-3">
                <a href="/profile/{{ $post->user->id }}" >
                    <img src="/storage/{{ $post->image }}" alt="" class="w-100">
                </a>
            </div>
        </div>
        <div class="row pt-2 pb-4">
            <div class="col-6 offset-3">
                <p>
                    <span class="font-weight-bold">
                        <a href="/profile/{{ $post->user->id }}">
                            <span class="text-dark">{{ $post->user->username }}</span>
                        </a>
                   </span>
                </p>
            </div>
        </div>
    @endforeach

    <div class="row">
        <div class="col-12 d-flex justify-content-center">
            {{ $posts->links()}}
        </div>
        
    </div>
</div>
@endsection
