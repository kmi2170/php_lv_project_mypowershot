<?php

namespace App\Http\Controllers;

use App\Post;
use Intervention\Image\Facades\Image;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PostsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $user_id = auth()->user()->id;
        $users = auth()->user()->following->pluck('user_id');

        /* $posts = Post::whereIn('user_id', $users)->orderBy('created_at', 'DESC')->get(); */
        /* $posts = Post::whereIn('user_id', $users)->latest()->get(); */
        /* $posts = Post::whereIn('user_id', $users)->latest()->paginate(5); */
        $posts = Post::whereIn('user_id', $users)->with('user')->latest()->paginate(5);

        /* dd($posts); */
        return view('posts.index', compact('posts', 'user_id'));
    }

    public function create()
    {
        /* return view('posts/create'); */
        return view('posts.create');
    }

    public function store()
    {
        $data= request()->validate([
            'caption'=> 'required',
            'image'=> [ 'required', 'image' ]
        ]);

        $imagePath=request('image')->store('uploads', 'public');

        /* dd(public_path("storage/{$imagePath}")); */
        $image = Image::make(public_path("storage/{$imagePath}"))->fit(1200, 1200);
        $image->save();

        /* auth()->user()->posts()->create($data); */
        auth()->user()->posts()->create([
            'caption' => $data['caption'],
            'image' => $imagePath,
        ]);

        /* \App\Post::create($data); */

        /* dd(request()->all()); */

        return redirect('/profile/'. auth()->user()->id);
    }

    /* public function show($post) */
    public function show(\App\Post $post)
    {
        /* dd($post); */
        return view('posts.show', compact('post'));
        /* return view( */
        /*     'posts.show', */
        /*     [ 'post' => $post ] */
        /* ); */
    }
}
